$(function(){

//===== Navigation button ===========

    $('.mobile-menu').on('click', function() {
        $('.main-navigation').slideToggle(350);
    });
    $('#nav-icon1').click(function(){
        $(this).toggleClass('open');
    });
//===================


$('.slide').slick({
    autoplay: true,
    arrows: false,
    dots: false,
    autoplaySpeed: 3000,
    pauseonHover:true,
    infinite: true
});

$('.product-slider__inner').slick({
    autoplay:true,
    autoplaySpeed:5000,
    speed: 500,
    infinite: true,
    dots:false,
    slidesToShow: 4,
    slidesToScroll: 4,
    centerMode: false,
    arrows:false,
    variableWidth:true,
    responsive: [
        {
            breakpoint: 1680,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                dots: false,
                arrows:false
            }
        },
        {
            breakpoint: 1280,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
                dots: false,
                arrows:false
            }
        },
        {
            breakpoint: 768,
            settings: "unslick"
        }
    ]
});

    $('.details__slider-block').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        fade: true,
        asNavFor: '.details__slider-carousel',
        arrows:false,
        dots:false
    });
    $('.details__slider-carousel').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        arrows:false,
        asNavFor: '.details__slider-block',
        dots: false,
        centerMode: true,
        focusOnSelect: true
    });

    $('#slider-left-arrow').click(function () {
        $('.product-slider__inner').slick('slickPrev');
    });
    $('#slider-right-arrow').click(function () {
        $('.product-slider__inner').slick('slickNext');
    });

   $('.widgets__slide').slick({
       autoplay:true,
       autoplaySpeed:5000,
       speed: 500,
       arrows:false,
       dots:false
   });

    $('.top-cart').click(function(){
        $(this).toggleClass('top-cart__active');
        $('.top-cart__inner').slideToggle(300);
    });
    $('.top-cart__block').on('click','.top-cart__delete',function(e){
       $(e.currentTarget).parent().parent().parent().remove();
        return false;
    });

    $('.mobile-form-button').click(function(){
        $('.mobile-form-block').slideToggle(400)
    });

    $('#second-button').click(function(){
        $('#first-button').removeClass('map-active');
        $('#second-button').addClass('map-active');
        $('#first-adress').hide();
        $('#second-adress').show();
    });
    $('#first-button').click(function(){
        $('#first-button').addClass('map-active');
        $('#second-button').removeClass('map-active');
        $('#second-adress').hide();
        $('#first-adress').show();
        return false;
    });
    $('.catalog__link').on('click',function(){
        $(this).toggleClass('catalog__opened');
        $(this).find('.catalog__dropped').slideToggle(300);
    });

    $('.catalog__mobile-button').click(function(){
        $('.catalog__sidebar').toggleClass('catalog__extra-height');
        $('.catalog__tree').slideToggle(300);
        $(this).toggleClass('catalog__mobile-opened');
    });
    $('.cart__delete').click(function(){
        $(this).parent().parent().remove();
    });
    $(".cart__increaseVal").click(function() {
        var input_el=$(this).prev('input');
        var v= input_el.val()*1+1;
        if(v<=input_el.attr('max'))
            input_el.val(v)
    });
    $(".cart__decreaseVal").click(function() {
        var input_el=$(this).next('input');
        var v= input_el.val()-1;
        if(v>=input_el.attr('min'))
            input_el.val(v)
    });

});


